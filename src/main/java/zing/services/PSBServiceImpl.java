package zing.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import zing.app.Locker;
import zing.app.PSBHTTPRequests;
import zing.exception.NotFoundException;
import zing.models.Answer;
import zing.models.Commission;
import zing.models.Gate;
import zing.models.Log;
import zing.models.psb.PSBCredentials;
import zing.models.psb.Result;
import zing.models.psb.psbcheck.InputRequest;
import zing.models.psb.psbcheck.ResponseCheckWithdraw;
import zing.models.psb.psbping.RequestPing;
import zing.models.psb.psbwithdraw.RequestWithdraw;
import zing.models.psb.psbwithdraw.ResponseWithdraw;
import zing.models.transaction.Transaction;
import zing.models.transaction.TransactionCommission;
import zing.repositories.GatesRepository;
import zing.repositories.ManualRepository;
import zing.repositories.TransactionsRepository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;

import static zing.models.psb.PSBCredentials.SOURCE_LOG;
import static zing.models.transaction.Status.*;

@Service
public class PSBServiceImpl implements RunnerService {
    private static final Logger logger = LogManager.getLogger("Main");
    private static final Logger pinglogger = LogManager.getLogger("Ping");
    private final Locker locker = Locker.getInstance();
    private final SimpleDateFormat timestamp = new SimpleDateFormat("yMMddHHmmss");
    private final PSBHTTPRequests psbhttpRequests;
    private Random rnd = new Random(System.currentTimeMillis());
    private final MongoTransactionManager mongoTransactionManager;
    private final RetryTemplate retryTemplate;

    {
        timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private final ManualRepository manualRepository;
    private final TransactionsRepository transactionsRepository;
    private final GatesRepository gatesRepository;

    public PSBServiceImpl(PSBHTTPRequests psbhttpRequests, MongoTransactionManager mongoTransactionManager, RetryTemplate retryTemplate, ManualRepository manualRepository
            , TransactionsRepository transactionsRepository, GatesRepository gatesRepository) {
        this.psbhttpRequests = psbhttpRequests;
        this.mongoTransactionManager = mongoTransactionManager;
        this.retryTemplate = retryTemplate;
        this.manualRepository = manualRepository;
        this.transactionsRepository = transactionsRepository;
        this.gatesRepository = gatesRepository;
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public CompletableFuture<ResponseWithdraw> withdraw(Transaction transaction) {
        Optional<Gate> gateOptional = gatesRepository.findById(PSBCredentials.GATE_ID_WITHDRAW);
        if (gateOptional.isEmpty())
            return CompletableFuture.completedFuture(null);
        Gate psb = gateOptional.get();

        rnd = new Random(System.currentTimeMillis());
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RequestWithdraw psbRequestWithdraw = RequestWithdraw.builder()
                .AMOUNT(String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100))
                .ORDER(transaction.getTransactionNumber().toString())
                .DESC(transaction.getPurpose())
                .TERMINAL(psb.getTerminal())
                .MERCH_NAME(psb.getMerchantName())
                .MERCHANT(psb.getMerchant())
                .EMAIL(transaction.getReceiverCredentials().getSenderEmail())
                .TIMESTAMP(timestamp.format(new Date()))
                .NONCE(Long.toHexString(rnd.nextLong()))
                .CARD(transaction.getReceiverCredentials().getCardNumber())
                .build();

        psbRequestWithdraw.createSign();

        return psbhttpRequests.withdrawPsb(psbRequestWithdraw, psb.getUrlWithdraw()).whenComplete((responseWithdraw, throwable) -> {
            try {
                retryTemplate.execute((RetryCallback<Void, Throwable>) context -> {
                    TransactionDefinition txDef = new DefaultTransactionDefinition();
                    TransactionStatus txStatus = mongoTransactionManager.getTransaction(txDef);
                    try {
                        handleWithdraw(responseWithdraw, throwable, transaction, txStatus);
                    } catch (NotFoundException ex) {
                        mongoTransactionManager.rollback(txStatus);
                        throw ex;
                    }
                    return null;
                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        });
    }


    @SuppressWarnings("DuplicatedCode")
    @Override
    public CompletableFuture<ResponseCheckWithdraw> checkTransaction(Transaction transaction) {
        Optional<Gate> gateOptional = gatesRepository.findById(PSBCredentials.GATE_ID_WITHDRAW);
        if (gateOptional.isEmpty())
            return CompletableFuture.completedFuture(null);
        Gate psb = gateOptional.get();

        rnd = new Random(System.currentTimeMillis());
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RequestWithdraw psbRequestWithdraw = RequestWithdraw.builder()
                .AMOUNT(String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100))
                .ORDER(transaction.getTransactionNumber().toString())
                .DESC(transaction.getPurpose())
                .TERMINAL(psb.getTerminal())
                .MERCH_NAME(psb.getMerchantName())
                .TRTYPE(transaction.getTrtype())
                .MERCHANT(psb.getMerchant())
                .EMAIL(transaction.getReceiverCredentials().getSenderEmail())
                .TIMESTAMP(timestamp.format(new Date()))
                .NONCE(Long.toHexString(rnd.nextLong()))
                .CARD(transaction.getReceiverCredentials().getCardNumber())
                .build();

        psbRequestWithdraw.createSign();

        try {
            return psbhttpRequests.checkPsb(psbRequestWithdraw, psb.getUrlCheck()).whenComplete((responseWithdraw, throwable) -> {
                try {
                    retryTemplate.execute((RetryCallback<Void, Throwable>) context -> {
                        TransactionDefinition txDef = new DefaultTransactionDefinition();
                        TransactionStatus txStatus = mongoTransactionManager.getTransaction(txDef);
                        try {
                            handleCheckWithdraw(responseWithdraw, transaction, txStatus);
                        } catch (NotFoundException ex) {
                            mongoTransactionManager.rollback(txStatus);
                            throw ex;
                        }
                        return null;
                    });
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            });
        } catch (
                Exception ex) {
            return CompletableFuture.completedFuture(null);
        }
    }

    @Override
    public void ping() {
        Optional<Gate> gateOpt = gatesRepository.findById(PSBCredentials.GATE_ID_WITHDRAW);
        if (gateOpt.isEmpty())
            throw new NotFoundException("Гейт ПСБ не найден!");
        Gate psb = gateOpt.get();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yMMddHHmmss");
        Random rnd = new Random(System.currentTimeMillis());

        RequestPing requestPing = RequestPing.builder()
                .TERMINAL(psb.getTerminal())
                .TIMESTAMP(dateFormat.format(new Date()))
                .NONCE(String.valueOf(rnd.nextLong()))
                .build();
        requestPing.createSign();


        psbhttpRequests.pingPsb(requestPing, psb.getUrlPing()).whenComplete((responsePing, throwable) -> {
            if (throwable != null)
                locker.unlock(Locker.PSB_PING_LOCK);
            if (responsePing.getERROR() == null) {
                Optional<Gate> gateOptional = gatesRepository.findById(PSBCredentials.GATE_ID_WITHDRAW);
                if (gateOptional.isEmpty())
                    try {
                        throw new NotFoundException("Гейт ПСБ не найден!");
                    } catch (NotFoundException e) {
                        e.printStackTrace();
                    }

                Gate psbGate = gateOptional.get();
                boolean isError = false;
                try {
                    psbGate.getBalance().setAmount((long) (responsePing.getAVAILABLE_AMOUNT().doubleValue() * 100));
                } catch (NumberFormatException ex) {
                    isError = true;
                    pinglogger.info(Log.builder()
                            .src(SOURCE_LOG)
                            .log(String.format("\nПри запросе баланса возникла ошибка." +
                                    "\nОперация - ping." +
                                    "\nОшибка - %s", ex.getMessage()))
                            .build());
                }

                if (!isError)
                    pinglogger.info(Log.builder()
                            .src(SOURCE_LOG)
                            .log("Баланс ПСБ обновлен")
                            .build());
                gatesRepository.save(psbGate);
            } else {
                pinglogger.info(Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("\nПри запросе баланса возникла ошибка." +
                                "\nОперация - ping." +
                                "\nОшибка - %s", responsePing.getERROR()))
                        .build());
            }

            locker.unlock(Locker.PSB_PING_LOCK);
        });
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public CompletableFuture<? extends Answer> checkTransactionInput(Transaction transaction) {
        Optional<Gate> gateOptional = gatesRepository.findById(PSBCredentials.GATE_ID_DONATE);
        if (gateOptional.isEmpty())
            return CompletableFuture.completedFuture(null);
        Gate psb = gateOptional.get();

        rnd = new Random(System.currentTimeMillis());
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            InputRequest inputRequest = InputRequest.builder()
                    .MERCH_NAME(psb.getMerchantName())
                    .MERCHANT(psb.getMerchant())
                    .TIMESTAMP(timestamp.format(new Date()))
                    .NONCE(Long.toHexString(rnd.nextLong()))
                    .EMAIL(transaction.getSenderCredentials().getSenderEmail())
                    .AMOUNT(String.valueOf(transaction.getFinalAmount().getAmount().doubleValue() / 100))
                    .ORDER(transaction.getTransactionNumber().toString())
                    .TERMINAL(psb.getTerminal())
                    .build();
            inputRequest.createSign();

            return psbhttpRequests.checkPsbDonate(inputRequest, psb.getUrlCheck()).whenComplete((responseWithdraw, throwable) -> {
                try {
                    retryTemplate.execute((RetryCallback<Void, Throwable>) context -> {
                        TransactionDefinition txDef = new DefaultTransactionDefinition();
                        TransactionStatus txStatus = mongoTransactionManager.getTransaction(txDef);
                        try {
                            handleInputTransaction(responseWithdraw, transaction, psb.getCommission().getBank(), txStatus);
                        } catch (NotFoundException ex) {
                            mongoTransactionManager.rollback(txStatus);
                            throw ex;
                        }
                        return null;
                    });
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception ex) {
            return CompletableFuture.completedFuture(null);
        }
    }

    private void handleWithdraw(ResponseWithdraw responseWithdraw, Throwable throwable, Transaction transaction, TransactionStatus txStatus) throws NotFoundException {
        if (throwable != null) {
            logger.info("Во время вывода возникла ошибка " + throwable.getMessage());
            transaction.setStage(0);
            transaction.setStatus(WAITING);
            transactionsRepository.save(transaction);
        }

        assert responseWithdraw != null;
        if (responseWithdraw.verifySign()) {
            if (responseWithdraw.getRESULT().equals(Result.SUCCESS) || responseWithdraw.getRESULT().equals(Result.REPEAT)) {
                transaction.setStage(2);
                transactionsRepository.save(transaction);

                mongoTransactionManager.commit(txStatus);
                logger.info(Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("\nТранзакция успешно создана" +
                                "\nНомер транзакции - %s." +
                                "\nОперация - withdraw.", transaction.getTransactionNumber()))
                        .build());

            } else {
                errorHandler(transaction, responseWithdraw.getRESULT(), responseWithdraw.getRCTEXT());
            }
        } else {
            declineTransaction(transaction, "Подпись от сервера не верна!");
        }
    }

    private void handleCheckWithdraw(ResponseCheckWithdraw responseWithdraw, Transaction transaction, TransactionStatus txStatus) throws NotFoundException {
        if (responseWithdraw.getRESULT() != null) {
            if (responseWithdraw.getRESULT().equals(Result.SUCCESS) && responseWithdraw.getRCTEXT().equals("Approved") && responseWithdraw.getTRTYPE().equals("70")) {
                transaction.setEndDate(new Date());
                transaction.setStatus(SUCCESS);

                transactionsRepository.save(transaction);
                if (transaction.getPartner() != null) {
                    manualRepository.updateCommissionWithRef(transaction);
                } else {
                    manualRepository.findAndUpdateGateCommission(transaction.getGateId(), transaction.getCommission());
                }

                mongoTransactionManager.commit(txStatus);
                logger.info(Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("\nТранзакция успешно выполнена" +
                                "\nНомер транзакции - %s." +
                                "\nОперация - checkWithdraw.", transaction.getTransactionNumber()))
                        .build());
            } else {
                try {
                    errorHandler(transaction, responseWithdraw.getRESULT(), responseWithdraw.getRCTEXT());
                } catch (NotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleInputTransaction(ResponseCheckWithdraw responseWithdraw, Transaction transaction, Commission commission, TransactionStatus txStatus) throws NotFoundException {
        if (responseWithdraw != null) {
            if (responseWithdraw.getRESULT().equals(Result.SUCCESS) && responseWithdraw.getRCTEXT().equals("Approved") && responseWithdraw.getTRTYPE().equals("1")) {
                transaction.setEndDate(new Date());
                transaction.setStatus(SUCCESS);

                if (manualRepository.findAndModifyGateAdd(transaction.getGateId(), transaction.getFinalAmount()) == null) {
                    logger.info(Log.builder()
                            .src(SOURCE_LOG)
                            .log(String.format("\nОшибка при выполнении транзакции - Гейт не найден" +
                                    "\nНомер транзакции - %s." +
                                    "\nОперация - checkInput.", transaction.getTransactionNumber()))
                            .build());

                    throw new NotFoundException("Gate с указанным ID не найден!");
                }
                if (manualRepository.findAndModifyAccountAdd(transaction.getReceiverCredentials().getAccountId(), transaction.getFinalAmount(), transaction.getId()) == null) {
                    logger.info(Log.builder()
                            .src(SOURCE_LOG)
                            .log(String.format("\nОшибка при выполнении транзакции - Аккаунт не найден" +
                                    "\nНомер транзакции - %s." +
                                    "\nОперация - checkInput.", transaction.getTransactionNumber()))
                            .build());

                    throw new NotFoundException("Account с указанным ID не найден!");
                }

                long baseCommission = ((long) (transaction.getFinalAmount().getAmount().longValue() * commission.getPercent())) + commission.getFee();
                long fullCommissionMin = baseCommission >= commission.getMin() ? baseCommission : commission.getMin();
                long fullCommission = fullCommissionMin;
                if (commission.getMax() > 0)
                    fullCommission = fullCommissionMin <= commission.getMax() ? fullCommissionMin : commission.getMax();

                transaction.setCommission(TransactionCommission.builder()
                        .bankAmount(fullCommission)
                        .systemAmount(0)
                        .currency(transaction.getFinalAmount().getCurrency())
                        .build());
                mongoTransactionManager.commit(txStatus);
                transactionsRepository.save(transaction);
                logger.info(Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("\nТранзакция успешно выполнена" +
                                "\nНомер транзакции - %s." +
                                "\nОперация - checkInput.", transaction.getTransactionNumber()))
                        .build());
            } else {
                errorHandler(transaction, responseWithdraw.getRESULT(), responseWithdraw.getRCTEXT());
            }
        } else {
            logger.info(Log.builder()
                    .src(SOURCE_LOG)
                    .log(String.format("\nОперация выполняется" +
                            "\nНомер транзакции - %s." +
                            "\nОперация - checkInput.", transaction.getTransactionNumber()))
                    .build());
        }
    }

    private void errorHandler(Transaction transaction, String result, String message) throws NotFoundException {
        switch (result) {
            case Result.REPEAT:
                transaction.setErrorReason("Запрос идентифицирован как повторный");
                break;
            case Result.BANK_DENIED:
                transaction.setErrorReason("Запрос отклонен Банком");
                break;
            case Result.SENDER_DENIED:
                transaction.setErrorReason("Запрос отклонен ПШ");
                break;
            default:
                transaction.setErrorReason("Неизвестный статус ошибки!");
        }
        declineTransaction(transaction, transaction.getErrorReason() + " " + message);
    }

    @Override
    public CompletableFuture<Transaction> declineTransaction(Transaction transaction, String errorCause) throws NotFoundException {
        if (transaction != null) {
            if ((manualRepository.findAndModifyAccountAdd(transaction.getSenderCredentials().getAccountId(), transaction.getFinalAmount(), transaction.getId()) != null)
                    && (manualRepository.findAndModifyGateAdd(transaction.getGateId(), transaction.getFinalAmount()) != null)) {
                transaction.setErrorReason(errorCause);
                transaction.setStatus(DENIED);
                transaction.setEndDate(new Date());

                logger.info(Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("\nТранзакция отменена" +
                                "\nНомер транзакции - %s." +
                                "\nОперация - decline." +
                                "\nПричина - %s", transaction.getTransactionNumber(), errorCause))
                        .build());

                transactionsRepository.save(transaction);
                return CompletableFuture.completedFuture(transaction);
            } else {
                logger.info(Log.builder()
                        .src(SOURCE_LOG)
                        .log("IN TRANSACTION SAVE gate with id dont find")
                        .build());
                throw new NotFoundException("IN TRANSACTION SAVE gate with id dont find");
            }
        } else {
            logger.info(Log.builder()
                    .src(SOURCE_LOG)
                    .log("IN TRANSACTION SAVE transaction dont find")
                    .build());
            throw new NotFoundException("IN TRANSACTION SAVE gate with id dont find");
        }
    }
}

