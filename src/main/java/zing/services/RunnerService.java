package zing.services;

import org.springframework.scheduling.annotation.Async;
import zing.models.Answer;
import zing.models.transaction.Transaction;

import java.util.concurrent.CompletableFuture;

@Async
public interface RunnerService {
    CompletableFuture<? extends Answer> withdraw(Transaction transaction);

    CompletableFuture<? extends Answer> checkTransaction(Transaction transaction);

    CompletableFuture<Transaction> declineTransaction(Transaction transaction, String errorCause);

    void ping();

    CompletableFuture<? extends Answer> checkTransactionInput(Transaction transaction);
}
