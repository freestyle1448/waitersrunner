package zing.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import zing.exception.NotAllowedException;
import zing.exception.NotFoundException;
import zing.models.Answer;
import zing.models.Gate;
import zing.models.Log;
import zing.models.payture.PayStatusResponse;
import zing.models.payture.PaytureCredentials;
import zing.models.psb.psbwithdraw.ResponseWithdraw;
import zing.models.transaction.PaytureLog;
import zing.models.transaction.Status;
import zing.models.transaction.Transaction;
import zing.repositories.GatesRepository;
import zing.repositories.ManualRepository;
import zing.repositories.TransactionsRepository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static zing.models.payture.PaytureCredentials.SOURCE_LOG;


@Service
public class PaytureServiceImpl implements RunnerService {
    private static final Logger logger = LogManager.getLogger("Main");
    private final RestTemplate restTemplate;
    private final ManualRepository manualRepository;
    private final GatesRepository gatesRepository;
    private final TransactionsRepository transactionsRepository;

    public PaytureServiceImpl(RestTemplate restTemplate, ManualRepository manualRepository, GatesRepository gatesRepository, TransactionsRepository transactionsRepository) {
        this.restTemplate = restTemplate;
        this.manualRepository = manualRepository;
        this.gatesRepository = gatesRepository;
        this.transactionsRepository = transactionsRepository;
    }

    @Override
    public CompletableFuture<ResponseWithdraw> withdraw(Transaction transaction) {
        throw new NotAllowedException("Not allowed in Payture Service");
    }

    @Override
    public CompletableFuture<Transaction> checkTransaction(Transaction transaction) {
        PayStatusResponse payStatusResponse = null;
        try {
            if (transaction.getPaytureLog() == null)
                transaction.setPaytureLog(PaytureLog.builder().build());
            transaction.getPaytureLog().setPayStatusRequest(transaction.getOrderId());
            payStatusResponse = payStatusRequest(transaction.getOrderId());
        } catch (JAXBException e) {
            transaction.getPaytureLog().setPayStatusResponse(e.getMessage());
            e.printStackTrace();
            transactionsRepository.save(transaction);
        }

        assert payStatusResponse != null;
        transaction.getPaytureLog().setPayStatusResponse(payStatusResponse.toString());
        if (payStatusResponse.getSuccess().equals("True")) {
            if (payStatusResponse.getState().equals(zing.models.payture.Status.CHARGED)) {
                if (manualRepository.findAndModifyGateAdd(PaytureCredentials.PAYTURE_GATE_ID, transaction.getFinalAmount()) != null &&
                        manualRepository.findAndModifyAccountAdd(transaction.getReceiverCredentials().getAccountId(), transaction.getFinalAmount(), transaction.getId()) != null) {
                    transaction.setStatus(Status.SUCCESS);
                    transaction.setEndDate(new Date());
                    transactionsRepository.save(transaction);
                    manualRepository.findAndUpdateGateCommission(transaction.getGateId(), transaction.getCommission());

                    logger.info(Log.builder()
                            .src(SOURCE_LOG)
                            .log(String.format("\nТранзакция успешно выполнена" +
                                    "\nНомер транзакции - %s." +
                                    "\nОперация - check.", transaction.getTransactionNumber()))
                            .build());

                    return CompletableFuture.completedFuture(transaction);
                } else {
                    logger.info(Log.builder()
                            .src(SOURCE_LOG)
                            .log(String.format("\nПри обработке ввода средств возникла ошибка." +
                                    "\nНомер транзакции - %s." +
                                    "\nОперация - check." +
                                    "\nОшибка - Гейт или аккаунт не найден!", transaction.getTransactionNumber()))
                            .build());

                    throw new NotFoundException("Гейт или аккаунт не найден!");
                }
            }
        } else if (!payStatusResponse.getErrCode().equals("NONE")){
            transaction.setStatus(Status.DENIED);
            transaction.setErrorReason(payStatusResponse.getErrCode());
            transaction.setEndDate(new Date());
            transactionsRepository.save(transaction);

            logger.info(Log.builder()
                    .src(SOURCE_LOG)
                    .log(String.format("\nПри обработке ввода средств возникла ошибка." +
                            "\nНомер транзакции - %s." +
                            "\nОперация - check." +
                            "\nОшибка - %s", transaction.getTransactionNumber(), payStatusResponse.getErrCode()))
                    .build());

            return CompletableFuture.completedFuture(transaction);
        }

        return CompletableFuture.completedFuture(transaction);
    }

    @Override
    public CompletableFuture<Transaction> declineTransaction(Transaction transaction, String errorCause) {
        return null;
    }

    @Override
    public void ping() {
        throw new NotAllowedException("Not allowed in Payture Service");
    }

    @Override
    public CompletableFuture<? extends Answer> checkTransactionInput(Transaction transaction) {
        throw new NotAllowedException("Not allowed in Payture Service");
    }

    private PayStatusResponse payStatusRequest(String orderId) throws JAXBException {
        Optional<Gate> paytureGateOptional = gatesRepository.findById(PaytureCredentials.PAYTURE_GATE_ID);
        Gate gate;
        if (paytureGateOptional.isEmpty())
            throw new NotFoundException("Гейт payture не найден");
        gate = paytureGateOptional.get();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map =
                new LinkedMultiValueMap<>();
        map.add("Key", gate.getMerchantKey());
        map.add("OrderId", orderId);

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(gate.getCurrentUrlPayStatus(), //Отправка запроса
                        entity,
                        String.class);

        JAXBContext jaxbContext = JAXBContext.newInstance(PayStatusResponse.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(Objects.requireNonNull(response.getBody() //Анмаршеллинг ответа
                , StandardCharsets.UTF_8.toString()));

        return (PayStatusResponse) jaxbUnmarshaller.unmarshal(reader);
    }
}
