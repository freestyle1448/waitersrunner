package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import zing.exception.NotFoundException;
import zing.models.Account;
import zing.models.Gate;
import zing.models.payture.PaytureCredentials;
import zing.models.psb.PSBCredentials;
import zing.models.transaction.*;

import java.util.Date;
import java.util.List;

import static zing.models.transaction.Status.*;
import static zing.models.transaction.Type.*;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Account findAndModifyAccountAdd(ObjectId accountId, Balance balance, ObjectId transactionId) {
        Query findAndModifyAccount = new Query(Criteria
                .where("_id").is(accountId)
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("holdBalance.amount", balance.getAmount().longValue());
        update.addToSet("holdBalance.transactions", transactionId);

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId)
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().longValue());


        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public List<Transaction> findPaytureTransaction() {
        Query query = new Query(Criteria.where("gateId").is(PaytureCredentials.PAYTURE_GATE_ID)
                .and("status").is(WAITING)
                .and("stage").is(0)
                .and("orderId").ne(null)
                .and("type").is(DONATE));

        return mongoTemplate.find(query, Transaction.class);
    }

    public Transaction findAndModifyPSBTransaction() {
        Query query = new Query(Criteria.where("gateId").is(PSBCredentials.GATE_ID_WITHDRAW)
                .and("status").is(WAITING)
                .and("stage").is(0)
                .and("type").is(WITHDRAWAL));
        Update update = new Update();
        update.set("status", IN_PROCESS);
        update.inc("stage", 1);

        return mongoTemplate.findAndModify(query, update, FindAndModifyOptions.options().returnNew(true), Transaction.class);
    }

    public List<Transaction> findPSBTransactions() {
        Query query = new Query(Criteria
                .where("status").is(IN_PROCESS)
                .and("stage").is(2)
                .and("gateId").is(PSBCredentials.GATE_ID_WITHDRAW)
                .and("type").is(WITHDRAWAL)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public List<Transaction> findPSBTransactionsDonate() {
        Query query = new Query(Criteria
                .where("status").is(IN_PROCESS)
                .and("stage").is(2)
                .and("gateId").is(PSBCredentials.GATE_ID_DONATE)
                .and("type").is(DONATE)
        );

        return mongoTemplate.find(query, Transaction.class);
    }

    public void rescue() {
        Query query = new Query(Criteria.where("gateId").is(PSBCredentials.GATE_ID_WITHDRAW)
                .and("status").is(IN_PROCESS)
                .and("stage").is(1)
                .and("type").is(WITHDRAWAL));
        Update update = new Update();
        update.set("status", WAITING);
        update.set("stage", 0);

        mongoTemplate.findAndModify(query, update, FindAndModifyOptions.options().returnNew(true), Transaction.class);
    }

    public void findAndUpdateGateCommission(ObjectId gateId, TransactionCommission balance) {
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId)
                .and("commissionBalance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("commissionBalance.amount", balance.getBankAmount().longValue());

        mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public void updateCommissionWithRef(Transaction transaction) {
        long refAmount = (long) (transaction.getFinalAmount().getAmount().longValue() * transaction.getPartner().getPercent());

        Balance refBalance = Balance.builder()
                .amount(refAmount)
                .currency(transaction.getFinalAmount().getCurrency())
                .build();

        Transaction transactionRef = Transaction.builder()
                .id(new ObjectId())
                .gateId(transaction.getGateId())
                .startDate(new Date())
                .transactionNumber(System.currentTimeMillis())
                .stage(0)
                .status(SUCCESS)
                .type(PARTNER_COMMISSION)
                .amount(refBalance)
                .finalAmount(refBalance)
                .senderCredentials(SenderCredentials.builder()
                        .parentId(transaction.getId())
                        .build())
                .build();

        Query findAndModifyAccount = new Query(Criteria
                .where("userId").is(new ObjectId(transaction.getPartner().getUserId()))
                .and("balance.currency").is(transaction.getFinalAmount().getCurrency()));
        Update updateAcc = new Update();
        updateAcc.inc("holdBalance.amount", refAmount);
        updateAcc.addToSet("holdBalance.transactions", transactionRef.getId());

        final Account andModify = mongoTemplate.findAndModify(findAndModifyAccount, updateAcc, Account.class);
        if (andModify == null)
            throw new NotFoundException("Account не найден!");

        transactionRef.setReceiverCredentials(ReceiverCredentials.builder()
                .account(andModify.getId())
                .build());

        TransactionCommission commission = transaction.getCommission();
        commission.setBankAmount(commission.getBankAmount().longValue() - refAmount);

        mongoTemplate.save(transactionRef, "transactions");

        Query findAndModifyGate = new Query(Criteria.where("_id").is(transaction.getGateId())
                .and("commissionBalance.currency").is(transaction.getCommission().getCurrency()));
        Update update = new Update();
        update.inc("commissionBalance.amount", transaction.getCommission().getBankAmount().longValue());

        mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }
}
