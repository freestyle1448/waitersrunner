package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.transaction.Transaction;

public interface TransactionsRepository extends MongoRepository<Transaction, ObjectId> {
}
