package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.Gate;

public interface GatesRepository extends MongoRepository<Gate, ObjectId> {
}

