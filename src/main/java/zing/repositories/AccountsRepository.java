package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.Account;

public interface AccountsRepository extends MongoRepository<Account, ObjectId> {
}
