package zing.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import zing.app.Locker;
import zing.models.Log;
import zing.models.transaction.Transaction;
import zing.repositories.ManualRepository;
import zing.services.RunnerService;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static zing.models.payture.PaytureCredentials.SOURCE_LOG;

@Async
@Component
public class PaytureTasks {
    private static final Logger logger = LogManager.getLogger("Main");
    private final Locker locker = Locker.getInstance();

    private final RunnerService paytureService;
    private final ManualRepository manualRepository;

    public PaytureTasks(@Qualifier("paytureServiceImpl") RunnerService paytureService, ManualRepository manualRepository) {
        this.paytureService = paytureService;
        this.manualRepository = manualRepository;
    }

    //@Scheduled(fixedRate = 5000, initialDelay = 2500)
    public void check() {
        if (locker.isNotLocked(Locker.PAYTURE_CHECK_LOCK)) {

            locker.lock(Locker.PAYTURE_CHECK_LOCK);
            try {
                List<Transaction> transactions = manualRepository.findPaytureTransaction();

                CompletableFuture<?>[] tasks = transactions.parallelStream()
                        .map(transaction -> {
                            if (true) {
                                try {
                                    return paytureService.checkTransaction(transaction).exceptionally(throwable -> {
                                        throwable.printStackTrace();

                                        logger.info(Log.builder()
                                                .src(SOURCE_LOG)
                                                .log(String.format("\nПри обработке ввода средств возникла ошибка." +
                                                        "\nНомер транзакции - %s." +
                                                        "\nОперация - check." +
                                                        "\nОшибка - %s", transaction.getTransactionNumber(), throwable.getMessage()))
                                                .build());
                                        return null;
                                    });
                                } catch (Exception e) {
                                    locker.unlock(Locker.PAYTURE_CHECK_LOCK);
                                    return null;
                                }
                            } else {
                                logger.info(Log.builder()
                                        .src(SOURCE_LOG)
                                        .log(String.format("\nПри обработке ввода средств возникла ошибка." +
                                                "\nНомер транзакции - %s." +
                                                "\nОперация - check." +
                                                "\nОшибка - неверный hash", transaction.getTransactionNumber()))
                                        .build());

                                return paytureService.declineTransaction(transaction, "Неверный hash!")
                                        .exceptionally(throwable -> {
                                            throwable.printStackTrace();
                                            return null;
                                        });
                            }
                        })
                        .toArray(CompletableFuture[]::new);

                CompletableFuture.allOf(tasks)
                        .thenRun(() -> locker.unlock(Locker.PAYTURE_CHECK_LOCK))
                        .get();
            } catch (Exception ex) {
                locker.unlock(Locker.PAYTURE_CHECK_LOCK);
            }
        }
    }
}
