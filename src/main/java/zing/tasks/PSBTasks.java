package zing.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import zing.app.Locker;
import zing.models.Log;
import zing.models.transaction.Transaction;
import zing.repositories.ManualRepository;
import zing.repositories.TransactionsRepository;
import zing.services.RunnerService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static zing.app.Locker.*;
import static zing.models.psb.PSBCredentials.SOURCE_LOG;
import static zing.models.transaction.Status.WAITING;

@Component
public class PSBTasks {
    private static final Logger logger = LogManager.getLogger("Main");
    private final Locker locker = Locker.getInstance();

    private final TransactionsRepository transactionsRepository;
    private final RunnerService psbService;
    private final ManualRepository manualRepository;

    public PSBTasks(TransactionsRepository transactionsRepository, @Qualifier("PSBServiceImpl") RunnerService psbService, ManualRepository manualRepository) {
        this.transactionsRepository = transactionsRepository;
        this.psbService = psbService;
        this.manualRepository = manualRepository;
    }

    @Scheduled(fixedRate = 15000)
    public void rescue() {
        if (locker.isNotLocked(PSB_PAY_LOCK)) {
            locker.lock(PSB_PAY_LOCK);
            manualRepository.rescue();

            locker.unlock(PSB_PAY_LOCK);
        }
    }

    @Scheduled(fixedRate = 20000)
    public void withdraw() throws ExecutionException, InterruptedException {
        if (locker.isNotLocked(PSB_PAY_LOCK)) {
            locker.lock(PSB_PAY_LOCK);

            List<Transaction> transactions = new ArrayList<>();
            Transaction tr = manualRepository.findAndModifyPSBTransaction();
            int counter = 0;
            while (tr != null && counter < 25) {
                counter++;
                transactions.add(tr);
                if (counter < 25)
                    tr = manualRepository.findAndModifyPSBTransaction();
            }

            CompletableFuture<?>[] tasks = transactions
                    .parallelStream()
                    .map(transaction -> {
                        try {
                            if (true) {
                                return psbService.withdraw(transaction).exceptionally(throwable -> {
                                    throwable.printStackTrace();
                                    return null;
                                });
                            } else {
                                logger.info(Log.builder()
                                        .src(SOURCE_LOG)
                                        .log(String.format("\nПри выводе средств возникла ошибка." +
                                                "\nНомер транзакции - %s." +
                                                "\nОперация - withdraw." +
                                                "\nОшибка - неверный hash", transaction.getTransactionNumber()))
                                        .build());
                                return psbService.declineTransaction(transaction, "Неверный hash!")
                                        .exceptionally(throwable -> {
                                            throwable.printStackTrace();
                                            return null;
                                        });
                            }
                        } catch (Exception exc) {
                            logger.info(Log.builder()
                                    .src(SOURCE_LOG)
                                    .log(String.format("\nПри выводе средств возникла ошибка." +
                                            "\nНомер транзакции - %s." +
                                            "\nОперация - withdraw." +
                                            "\nОшибка - %s", transaction.getTransactionNumber(), exc.getMessage()))
                                    .build());
                            transaction.setStage(0);
                            transaction.setStatus(WAITING);

                            transactionsRepository.save(transaction);
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture
                    .allOf(tasks)
                    .thenRun(() -> locker.unlock(PSB_PAY_LOCK))
                    .get();
        }
    }

    @Scheduled(fixedRate = 25000, initialDelay = 5000)
    public void checkTransaction() throws ExecutionException, InterruptedException {
        if (locker.isNotLocked(PSB_CONFIRM_LOCK)) {
            locker.lock(PSB_CONFIRM_LOCK);

            List<Transaction> transactionsAll = manualRepository.findPSBTransactions();

            List<Transaction> transactions;
            if (transactionsAll.size() > 26)
                transactions = transactionsAll.subList(0, 25);
            else
                transactions = transactionsAll;

            CompletableFuture<?>[] tasks = transactions.parallelStream()
                    .map(transaction -> {
                        try {
                            if (true) {
                                return psbService.checkTransaction(transaction).exceptionally(throwable -> {
                                    throwable.printStackTrace();
                                    return null;
                                });
                            } else {
                                return psbService.declineTransaction(transaction, "Неверный hash!")
                                        .exceptionally(throwable -> {
                                            throwable.printStackTrace();
                                            return null;
                                        });
                            }
                        } catch (Exception exc) {
                            logger.info(Log.builder()
                                    .src(SOURCE_LOG)
                                    .log(String.format("\nПри обработке вывода средств возникла ошибка." +
                                            "\nНомер транзакции - %s." +
                                            "\nОперация - check." +
                                            "\nОшибка - неверный hash", transaction.getTransactionNumber()))
                                    .build());
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .thenRun(() -> locker.unlock(PSB_CONFIRM_LOCK))
                    .get();
        }
    }

    @Scheduled(fixedRate = 60000, initialDelay = 10000)
    public void ping() {
        if (locker.isNotLocked(PSB_PING_LOCK)) {
            locker.lock(PSB_PING_LOCK);
            try {
                psbService.ping();
            } catch (Exception ex) {
                locker.unlock(PSB_PING_LOCK);
                logger.info(Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("\nПри запросе баланса возникла ошибка." +
                                "\nОперация - ping." +
                                "\nОшибка - %s", ex.getMessage()))
                        .build());
            }
        }
    }

    @Scheduled(fixedRate = 20000)
    public void inputCheck() throws ExecutionException, InterruptedException {
        if (locker.isNotLocked(PSB_INPUT_LOCK)) {
            locker.lock(PSB_INPUT_LOCK);

            List<Transaction> transactionsAll = manualRepository.findPSBTransactionsDonate();

            List<Transaction> transactions;
            if (transactionsAll.size() > 26)
                transactions = transactionsAll.subList(0, 25);
            else
                transactions = transactionsAll;

            CompletableFuture<?>[] tasks = transactions.parallelStream()
                    .map(transaction -> {
                        try {
                            if (true) {
                                return psbService.checkTransactionInput(transaction).exceptionally(throwable -> {
                                    throwable.printStackTrace();
                                    return null;
                                });
                            } else {
                                return psbService.declineTransaction(transaction, "Неверный hash!")
                                        .exceptionally(throwable -> {
                                            throwable.printStackTrace();
                                            return null;
                                        });
                            }
                        } catch (Exception exc) {
                            logger.info(Log.builder()
                                    .src(SOURCE_LOG)
                                    .log(String.format("\nПри обработке вывода средств возникла ошибка." +
                                            "\nНомер транзакции - %s." +
                                            "\nОперация - check." +
                                            "\nОшибка - неверный hash", transaction.getTransactionNumber()))
                                    .build());
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .thenRun(() -> locker.unlock(PSB_INPUT_LOCK))
                    .get();
        }
    }
}
