package zing.models.psb;

import org.bson.types.ObjectId;

public final class PSBCredentials {
    //public static final String PUBLIC_KEY_PATH = "test/pubkey.der";
    //public static final String PRIVATE_KEY_PATH = "test/private_key.der";
    public static final String PUBLIC_KEY_PATH = "prod/pubkey.der";
    public static final String PRIVATE_KEY_PATH = "prod/private_key.der";
    public static final ObjectId GATE_ID_WITHDRAW = new ObjectId("5d5db34e1cea661e657c09d1");
    public static final ObjectId GATE_ID_DONATE = new ObjectId("5dbc1a1bdb8d50042ca681aa");
    public static final String SOURCE_LOG = "PSB";
}
