package zing.models.psb.psbwithdraw;

import lombok.*;
import zing.app.CryptoTools;
import zing.models.Answer;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseWithdraw extends Answer {
    private String AMOUNT;
    private String CURRENCY;
    private String ORDER;
    private String DESC;
    private String TERMINAL;
    private String TRTYPE;
    private String MERCH_NAME;
    private String MERCHANT;
    private String EMAIL;
    private String TIMESTAMP;
    private String NONCE;
    private String BACKREF;
    private String RESULT;
    private String RC;
    private String RCTEXT;
    private String AUTHCODE;
    private String RRN;
    private String INT_REF;
    private String P_SIGN;
    private String NAME;
    private String CARD;

    public boolean verifySign() {
        StringBuilder signBuilder = new StringBuilder();
        signBuilder.append(AMOUNT != null && !AMOUNT.isEmpty() ? String.format("%d%s", AMOUNT.length(), AMOUNT) : "-");
        signBuilder.append(CURRENCY != null && !CURRENCY.isEmpty() ? String.format("%d%s", CURRENCY.length(), CURRENCY) : "-");
        signBuilder.append(ORDER != null && !ORDER.isEmpty() ? String.format("%d%s", ORDER.length(), ORDER) : "-");
        signBuilder.append(MERCH_NAME != null && !MERCH_NAME.isEmpty() ? String.format("%d%s", MERCH_NAME.length(), MERCH_NAME) : "-");
        signBuilder.append(MERCHANT != null && !MERCHANT.isEmpty() ? String.format("%d%s", MERCHANT.length(), MERCHANT) : "-");
        signBuilder.append(TERMINAL != null && !TERMINAL.isEmpty() ? String.format("%d%s", TERMINAL.length(), TERMINAL) : "-");
        signBuilder.append(EMAIL != null && !EMAIL.isEmpty() ? String.format("%d%s", EMAIL.length(), EMAIL) : "-");
        signBuilder.append(TRTYPE != null && !TRTYPE.isEmpty() ? String.format("%d%s", TRTYPE.length(), TRTYPE) : "-");
        signBuilder.append(TIMESTAMP != null && !TIMESTAMP.isEmpty() ? String.format("%d%s", TIMESTAMP.length(), TIMESTAMP) : "-");
        signBuilder.append(NONCE != null && !NONCE.isEmpty() ? String.format("%d%s", NONCE.length(), NONCE) : "-");
        signBuilder.append(BACKREF != null && !BACKREF.isEmpty() ? String.format("%d%s", BACKREF.length(), BACKREF) : "-");
        signBuilder.append(RESULT != null && !RESULT.isEmpty() ? String.format("%d%s", RESULT.length(), RESULT) : "-");
        signBuilder.append(RC != null && !RC.isEmpty() ? String.format("%d%s", RC.length(), RC) : "-");
        signBuilder.append(RCTEXT != null && !RCTEXT.isEmpty() ? String.format("%d%s", RCTEXT.length(), RCTEXT) : "-");
        signBuilder.append(AUTHCODE != null && !AUTHCODE.isEmpty() ? String.format("%d%s", AUTHCODE.length(), AUTHCODE) : "-");
        signBuilder.append(RRN != null && !RRN.isEmpty() ? String.format("%d%s", RRN.length(), RRN) : "-");
        signBuilder.append(INT_REF != null && !INT_REF.isEmpty() ? String.format("%d%s", INT_REF.length(), INT_REF) : "-");

        return CryptoTools.verify(signBuilder.toString().getBytes(), P_SIGN);
    }
}
