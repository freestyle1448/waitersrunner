package zing.models.psb.psbping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zing.app.CryptoTools;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestPing {
    private String TERMINAL;
    private String TIMESTAMP;
    private String NONCE;
    private String P_SIGN;

    public void createSign() {
        StringBuilder signBuilder = new StringBuilder();
        signBuilder.append(TERMINAL != null && !TERMINAL.isEmpty() ? String.format("%d%s", TERMINAL.length(), TERMINAL) : "-");
        signBuilder.append(TIMESTAMP != null && !TIMESTAMP.isEmpty() ? String.format("%d%s", TIMESTAMP.length(), TIMESTAMP) : "-");
        signBuilder.append(NONCE != null && !NONCE.isEmpty() ? String.format("%d%s", NONCE.length(), NONCE) : "-");

        this.P_SIGN = CryptoTools.sign(signBuilder.toString().getBytes());
    }
}
