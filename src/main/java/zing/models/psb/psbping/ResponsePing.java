package zing.models.psb.psbping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponsePing {
    private Number AVAILABLE_AMOUNT;
    private String ERROR;

}
