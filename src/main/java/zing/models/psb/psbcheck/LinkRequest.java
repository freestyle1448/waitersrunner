package zing.models.psb.psbcheck;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class LinkRequest {
    private String AMOUNT;
    @Builder.Default
    private String CURRENCY = "RUB";
    private String DESC;
    private String TERMINAL;
    @Builder.Default
    private String TRTYPE = "1";
    @Builder.Default
    private String BACKREF = "https://zingpay.ru";
    private String ORDER;
    private String EMAIL;
    @Builder.Default
    private String MERCHANT_NOTIFY_EMAIL = "freestyle1448@gmail.com";
    @Builder.Default
    private String ADDINFO = "оплата";
    private String NOTIFY_URL;
    private String P_SIGN;

}
