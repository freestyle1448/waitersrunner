package zing.models.psb.psbcheck;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.lang.reflect.Field;

import static zing.app.CryptoTools.genHash;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InputRequest {
    private String AMOUNT;
    @Builder.Default
    private String CURRENCY = "RUB";
    private String ORDER;
    private String DESC;
    private String TERMINAL;
    @Builder.Default
    private String TRTYPE = "1";
    private String MERCH_NAME;
    private String MERCHANT;
    private String EMAIL;
    private String TIMESTAMP;
    private String NONCE;
    @Builder.Default
    private String BACKREF = "https://zingpay.ru";
    private String NOTIFY_URL;
    private String P_SIGN;

    @SuppressWarnings("DuplicatedCode")
    public void createSign() {
        StringBuilder signBuilder = new StringBuilder();
        signBuilder.append(AMOUNT != null && !AMOUNT.isEmpty() ? String.format("%d%s", AMOUNT.length(), AMOUNT) : "-");
        signBuilder.append(CURRENCY != null && !CURRENCY.isEmpty() ? String.format("%d%s", CURRENCY.length(), CURRENCY) : "-");
        signBuilder.append(ORDER != null && !ORDER.isEmpty() ? String.format("%d%s", ORDER.length(), ORDER) : "-");
        signBuilder.append(MERCH_NAME != null && !MERCH_NAME.isEmpty() ? String.format("%d%s", MERCH_NAME.length(), MERCH_NAME) : "-");
        signBuilder.append(MERCHANT != null && !MERCHANT.isEmpty() ? String.format("%d%s", MERCHANT.length(), MERCHANT) : "-");
        signBuilder.append(TERMINAL != null && !TERMINAL.isEmpty() ? String.format("%d%s", TERMINAL.length(), TERMINAL) : "-");
        signBuilder.append(EMAIL != null && !EMAIL.isEmpty() ? String.format("%d%s", EMAIL.length(), EMAIL) : "-");
        signBuilder.append(TRTYPE != null && !TRTYPE.isEmpty() ? String.format("%d%s", TRTYPE.length(), TRTYPE) : "-");
        signBuilder.append(TIMESTAMP != null && !TIMESTAMP.isEmpty() ? String.format("%d%s", TIMESTAMP.length(), TIMESTAMP) : "-");
        signBuilder.append(NONCE != null && !NONCE.isEmpty() ? String.format("%d%s", NONCE.length(), NONCE) : "-");
        signBuilder.append(BACKREF != null && !BACKREF.isEmpty() ? String.format("%d%s", BACKREF.length(), BACKREF) : "-");

        this.P_SIGN = genHash(signBuilder.toString());
    }


    @SuppressWarnings("DuplicatedCode")
    public MultiValueMap<String, String> requestData() throws IllegalAccessException {
        MultiValueMap<String, String> map =
                new LinkedMultiValueMap<>();

        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.get(this) == null)
                map.add(field.getName(), "NONE");
            map.add(field.getName(), String.valueOf(field.get(this)));
        }

        return map;
    }
}
