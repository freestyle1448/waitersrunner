package zing.models.psb.psbcheck;

import lombok.*;
import zing.models.Answer;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseCheckWithdraw extends Answer {
    private String AMOUNT;
    private String ORG_AMOUNT;
    private String CURRENCY;
    private String ORDER;
    private String DESC;
    private String MERCH_NAME;
    private String MERCHANT;
    private String TERMINAL;
    private String EMAIL;
    private String TRTYPE;
    private String TIMESTAMP;
    private String NONCE;
    private String BACKREF;
    private String RESULT;
    private String RC;
    private String RCTEXT;
    private String AUTHCODE;
    private String RRN;
    private String INT_REF;
    private String NAME;
    private String P_SIGN;
    private String CARD;
}
