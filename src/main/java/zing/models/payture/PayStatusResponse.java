package zing.models.payture;

import lombok.*;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@XmlRootElement(name = "PayStatus")
public class PayStatusResponse {
    @NonNull
    @XmlAttribute(name = "Success")
    private String Success;
    @NonNull
    @XmlAttribute(name = "OrderId")
    private String OrderId;
    @NonNull
    @XmlAttribute(name = "MerchantContract")
    private String MerchantContract;
    @XmlAttribute(name = "FinalTerminal")
    private String FinalTerminal;
    @XmlAttribute(name = "Amount")
    private Long Amount;
    @XmlAttribute(name = "State")
    private String State;
    @XmlAttribute(name = "ErrCode")
    private String ErrCode;
}
