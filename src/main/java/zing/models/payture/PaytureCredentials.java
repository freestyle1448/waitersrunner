package zing.models.payture;

import org.bson.types.ObjectId;

public final class PaytureCredentials {
    public static final String merchantKey = "MerchantZing";
    public static final ObjectId PAYTURE_GATE_ID = new ObjectId("5d5dc87b1cea661e658a7d81");
    static final String password = "123";
    public static final String PRODUCT = "чаевые";
    public static final String BACKREF_URL = "";
    public static final String SOURCE_LOG = "PAYTURE";
}
