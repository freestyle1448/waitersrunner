package zing.models.transaction;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import zing.models.Answer;
import zing.models.Partner;
import zing.models.payture.Cheque;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "transactions")
public
class Transaction extends Answer {
    @Id
    private ObjectId id;
    private String hash;
    private String sign;
    private String salt;
    private ObjectId gateId;
    private Date startDate;
    private Date endDate;
    private Long transactionNumber;
    private Integer status;
    private Integer stage;
    private String type;
    private Integer gateType;
    private Balance amount;
    private TransactionCommission commission;
    private Balance finalAmount;
    private String purpose;
    private String errorReason;
    private String errorUserReason;
    private String orderId;
    private ReceiverCredentials receiverCredentials;
    private SenderCredentials senderCredentials;
    private PaytureLog paytureLog;
    private String nonce;
    private Cheque cheque;
    private Partner partner;
    private String trtype;
}
