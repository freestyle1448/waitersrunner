package zing.models.transaction;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class TransactionDTO {
    private final Gson gson = new Gson();

    private String account_id;
    private String gateId;
    private String amount;
    private String bankCommission;
    private String systemCommission;
    private String finalAmount;
    private String systemRate;
    private String note;
    private String receiverCredentials;
    private String senderCredentials;
    private String sign;
    private String salt;

    public Transaction createTransaction(String type) {
        return Transaction.builder()
                .sign(sign)
                .salt(salt)
                .gateId(gateId != null ? new ObjectId(gateId) : null)
                .startDate(new Date())
                .transactionNumber(System.currentTimeMillis())
                .status(Status.WAITING)
                .stage(0)
                .type(type)
                .amount(amount != null ? Balance.builder()
                        .amount(Long.valueOf(amount))
                        .build() : null)
                .commission(bankCommission != null && systemCommission != null ? TransactionCommission.builder()
                        .bankAmount(Long.valueOf(bankCommission))
                        .systemAmount(Long.valueOf(systemCommission))
                        .currency("RUB")
                        .build() : TransactionCommission.builder()
                        .bankAmount(0)
                        .systemAmount(0)
                        .currency("RUB")
                        .build())
                .finalAmount(finalAmount != null ? Balance.builder()
                        .amount(Long.valueOf(finalAmount))
                        .build() : null)
                .purpose(note)
                .receiverCredentials(receiverCredentials != null ? gson.fromJson(receiverCredentials, ReceiverCredentials.class) : null)
                .senderCredentials(senderCredentials != null ? gson.fromJson(senderCredentials, SenderCredentials.class) : null)
                .build();
    }
}
