package zing.exception;

import com.mongodb.MongoException;

class NotEnoughMoneyException extends MongoException {
    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
