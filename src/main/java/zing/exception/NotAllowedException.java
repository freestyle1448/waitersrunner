package zing.exception;

import com.mongodb.MongoException;

public class NotAllowedException extends MongoException {
    public NotAllowedException(String message) {
        super(message);
    }
}
