package zing.exception;

import com.mongodb.MongoException;

class IllegalValueException extends MongoException {
    public IllegalValueException(String message) {
        super(message);
    }
}
