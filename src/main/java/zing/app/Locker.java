package zing.app;

import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;

public class Locker {
    public static final String PAYTURE_CHECK_LOCK = "payture_check.lock";

    public static final String PSB_PING_LOCK = "psb_ping.lock";
    public static final String PSB_PAY_LOCK = "psb_pay.lock";
    public static final String PSB_CONFIRM_LOCK = "psb_confirm.lock";
    public static final String PSB_INPUT_LOCK = "psb_input.lock";

    private static String PATH;//"/home/admin/"

    static {
        try {
            PATH = new ClassPathResource("application.properties").getURL().toString().replace("file:", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Locker instance;

    private Locker() {
    }

    public static synchronized Locker getInstance() {

        if (instance == null) {
            instance = new Locker();
        }
        return instance;
    }

    public boolean isNotLocked(String lockType) {
        File file = new File(String.format("%s%s", PATH, lockType));
        return !file.exists() || !file.isFile();
    }

    public void lock(String lockType) {
        File file = new File(String.format("%s%s", PATH, lockType));
        try {
            boolean t = file.createNewFile();
            if (t)
                System.out.println("LOCKED " + lockType);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unlock(String lockType) {
        File file = new File(String.format("%s%s", PATH, lockType));
        boolean t = file.delete();
        if (t)
            System.out.println("UNLOCKED " + lockType);
    }
}
