package zing.app;

import org.springframework.core.io.ClassPathResource;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import static zing.models.psb.PSBCredentials.PRIVATE_KEY_PATH;
import static zing.models.psb.PSBCredentials.PUBLIC_KEY_PATH;

public final class CryptoTools {
    public static String sign(byte[] textToHash) {
        try {
            FileInputStream inpriv = new FileInputStream(new File(new ClassPathResource(PRIVATE_KEY_PATH).getURI()));
            byte[] bufferPriv = new byte[inpriv.available()];
            inpriv.read(bufferPriv);
            inpriv.close();

            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(bufferPriv);

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = kf.generatePrivate(ks);

            byte[] signature;

            Signature sig = Signature.getInstance("MD5withRSA");
            sig.initSign(privateKey);
            sig.update(textToHash);
            signature = sig.sign();
            assert signature != null;
            return DatatypeConverter.printHexBinary(signature);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static boolean verify(byte[] data, String hexSign) {
        byte[] sign = DatatypeConverter.parseHexBinary(hexSign);
        try {
            FileInputStream inpub = new FileInputStream(new File(new ClassPathResource(PUBLIC_KEY_PATH).getURI()));
            byte[] bufferPub = new byte[inpub.available()];
            inpub.read(bufferPub);
            inpub.close();

            X509EncodedKeySpec pks = new X509EncodedKeySpec(bufferPub);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey publicKey = kf.generatePublic(pks);

            Signature signature = Signature.getInstance("MD5withRSA");

            signature.initVerify(publicKey);
            signature.update(data);

            return signature.verify(sign);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static String genHash(String stringToHash) {
        Process process;
        try {
            process = Runtime.getRuntime().exec(new String[]{"php", "/opt/tomcat/latest/webapps/sign.php", "34833044E2AA7E0039A42517119DE664", stringToHash});

            process.waitFor();

            String line;
            String hash = null;

            BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            while ((line = outputReader.readLine()) != null) {
                hash = line.trim();
            }

            outputReader.close();
            return hash;

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
