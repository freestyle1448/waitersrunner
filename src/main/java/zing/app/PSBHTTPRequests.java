package zing.app;

import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import zing.models.psb.psbcheck.InputRequest;
import zing.models.psb.psbcheck.ResponseCheckWithdraw;
import zing.models.psb.psbping.RequestPing;
import zing.models.psb.psbping.ResponsePing;
import zing.models.psb.psbwithdraw.RequestWithdraw;
import zing.models.psb.psbwithdraw.ResponseWithdraw;

import java.util.concurrent.CompletableFuture;

@Component
@Async("threadPoolTaskExecutor")
public class PSBHTTPRequests {
    private final RestTemplate restTemplate;
    private final Gson gson = new Gson();

    public PSBHTTPRequests(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CompletableFuture<ResponseWithdraw> withdrawPsb(RequestWithdraw requestWithdraw, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = null;
        try {
            map = requestWithdraw.requestData();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(url, //Отправка запроса
                        entity,
                        String.class);

        return CompletableFuture.completedFuture(gson.fromJson(response.getBody(), ResponseWithdraw.class));
    }

    @SuppressWarnings("Duplicates")
    public CompletableFuture<ResponseCheckWithdraw> checkPsb(RequestWithdraw requestWithdraw, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = null;
        try {
            map = requestWithdraw.requestData();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        try {
            ResponseEntity<String> response =
                    restTemplate.postForEntity(url, //Отправка запроса
                            entity,
                            String.class);

            return CompletableFuture.completedFuture(gson.fromJson(response.getBody(), ResponseCheckWithdraw.class));
        } catch (Exception ex) {
            ex.printStackTrace();
            return CompletableFuture.completedFuture(gson.fromJson("{}", ResponseCheckWithdraw.class));
        }
    }

    @SuppressWarnings("Duplicates")
    public CompletableFuture<ResponseCheckWithdraw> checkPsbDonate(InputRequest requestWithdraw, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = null;
        try {
            map = requestWithdraw.requestData();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        try {
            ResponseEntity<String> response =
                    restTemplate.postForEntity(url, //Отправка запроса
                            entity,
                            String.class);

            return CompletableFuture.completedFuture(gson.fromJson(response.getBody(), ResponseCheckWithdraw.class));
        } catch (Exception ex) {
            ex.printStackTrace();
            return CompletableFuture.completedFuture(gson.fromJson("{}", ResponseCheckWithdraw.class));
        }
    }

    public CompletableFuture<ResponsePing> pingPsb(RequestPing requestPing, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map =
                new LinkedMultiValueMap<>();
        map.add("TERMINAL", requestPing.getTERMINAL());
        map.add("TIMESTAMP", requestPing.getTIMESTAMP());
        map.add("NONCE", requestPing.getNONCE());
        map.add("P_SIGN", requestPing.getP_SIGN());

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(url, //Отправка запроса
                        entity,
                        String.class);

        return CompletableFuture.completedFuture(gson.fromJson(response.getBody(), ResponsePing.class));
    }
}
